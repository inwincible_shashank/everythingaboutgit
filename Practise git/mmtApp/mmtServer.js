
const express = require('express');
const bodyParser = require('body-parser');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

// Initialize Express app
const app = express();
const port = 3000;

// Middleware
app.use(bodyParser.json());

// Dummy user data (replace this with database logic)
const users = [
    {
        id: 1,
        email: 'user@example.com',
        password: '$2a$10$D9mHS9J8HYknjOIj0mCLhOM7aOcRdW9J79yx7zYZGkJUD.NpZrVWe2', // bcrypt hash of 'password123'
    }
];

// Secret key for JWT
const JWT_SECRET = 'your_jwt_secret_key';

// Login API endpoint
app.post('/login', async (req, res) => {
    const { email, password } = req.body;

    // Find the user by email
    const user = users.find(u => u.email === email);
    if (!user) {
        return res.status(401).json({ message: 'Invalid email or password' });
    }

    // Check if the password matches
    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
        return res.status(401).json({ message: 'Invalid email or password' });
    }

    // Create a JWT token
    const token = jwt.sign({ userId: user.id, email: user.email }, JWT_SECRET, { expiresIn: '1h' });

    // Respond with the token
    res.json({ token });
});

// Start the server
app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
