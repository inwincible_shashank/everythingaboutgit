
Feature: Failed Scenarios for end to end web testing live site

  Scenario: TC-Login-05
    Given User is on the login page
    When User enters invalid OTP
    And User submits the form
    Then User should see an OTP failed message

  Scenario: TC-Login-08
    Given User is on the login page
    When User enters incorrect password multiple times
    Then User account should be locked for 30 minutes

  Scenario: TC-Homepage-01
    Given User is on the homepage
    When User interacts with the chatbot
    And User submits a refund ticket
    Then User should see a message that refund ticket cannot be submitted

  Scenario: TC-Search-03
    Given User is on the search page
    When User searches for a non-existent item
    Then User should see a "No results found" message

  Scenario: TC-Checkout-07
    Given User has items in the cart
    When User proceeds to checkout
    And User enters an invalid credit card number
    Then User should see a payment failure message
