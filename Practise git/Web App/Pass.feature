
Feature: Successful Scenarios for end to end web testing live site

  Scenario: TC-Login-01
    Given User is on the login page
    When User enters valid credentials
    And User submits the form
    Then User should be redirected to the dashboard

  Scenario: TC-Homepage-02
    Given User is on the homepage
    When User clicks on the profile link
    Then User should be taken to the profile page

  Scenario: TC-Search-01
    Given User is on the search page
    When User searches for an existing item
    Then User should see relevant search results

  Scenario: TC-Checkout-01
    Given User has items in the cart
    When User proceeds to checkout
    And User enters a valid credit card number
    Then User should see a payment successful message

  Scenario: TC-Profile-01
    Given User is on the profile page
    When User updates their email with a valid format
    Then User should see a confirmation message indicating email updated successfully
