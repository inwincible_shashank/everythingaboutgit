![Logo](logo.jpg)

## Project Title
This repository provides a comprehensive guide to git

## Authors
[@inwincible_shashank](https://gitlab.com/inwincible_shashank/)

## Purpose
To educate fellow QA's on git so that they can deploy their automation project files, folders and collabrate with other team members

## Prerequisites
- Install git bash
- Create account on gitlab

## Download and Installation
1. Download & Install gitbash from here - https://www.git-scm.com/download/win
2. Under Standalone Installer
3. Click on "64-bit Git for Windows Setup" link
4. .exe file will be downloaded
5. Then install the same on your windows machine
6. Then in your project explorer create a new empty folder
7. Left click and open gitbash here
8. Execute below commands
         
```
  git clone git@gitlab.com:inwincible_shashank/everythingaboutgit.git
```

## Project Explanation
1. README.md file is the start point for the download, installation
2. Then start learning and performing steps from below #Quick Start
3. When you practise these git cmds on your local using content from individual chapters, you will only need the folder "Practise git".

### IMP Note:
- When we want to track project files, folders do NOT initialise '.git' folder for Parent as well as Child. Either do it for Parent folder or Child folder.
- I need to upload all my project files, folders inside 'everythingaboutgit' hence I have created '.git' folder inside that folder level.
- But when you are practising these commands create 'Practise git' a Parent folder And create '.git' folder inside that

*(Do NOT keep the other files 'logo.png', 'README.md' and folders 'Chapters', 'Screenshots'. I have used this for proeject creation and to demonstrate it's content so refering that you can learn.)*

## Quick Start
To get started quickly, follow below chapters:
1. [Chapter1: Basic Commands](Chapters/Chapter1-Basics.md)
2. [Chapter2: git init](Chapters/Chapter2-git%20init.md)
3. [Chapter3: git status](Chapters/Chapter3-git%20status.md)
4. [Chapter4: git add](Chapters/Chapter4-git%20add.md)
5. [Chapter5: git commit](Chapters/Chapter5-git%20commit.md)
6. [Chapter6: git log](Chapters/Chapter6-git%20log.md)
7. [Chapter7: git reset](Chapters/Chapter7-git%20reset.md)
8. [Chapter8: git revert](Chapters/Chapter8-git%20revert.md)
9. [Chapter9: git local branches](Chapters/Chapter9-Local%20Branches.md)
10. [Chapter10: git merge (Part-1)](Chapters/Chapter10-git%20merge%20(part1).md)
11. [Chapter11: git merge (Part-2)](Chapters/Chapter11-git%20merge%20(part2).md)
12. [Chapter12: git cherry-pick](Chapters/Chapter12-git%20cherry-pick.md)
13. [Chapter13: git remote (Part-1)](Chapters/Chapter13-git%20remote%20(part1).md)
14. [Chapter14: git remote (Part-2)](Chapters/Chapter14-git%20remote%20(part2).md)
15. [Chapter15: git remote branches](Chapters/Chapter15-git%20remote%20branches.md)
16. [Chapter16: git pull](Chapters/Chapter16-git%20pull.md)
17. [Chapter17: git pull v/s git fetch-git merge](Chapters/Chapter17-git%20fetch-git%20merge.md)
18. [Chapter18: git stash](Chapters/Chapter18-git%20stash.md)
19. [Chapter19: git ignore](Chapters/Chapter19-git%20ignore.md)
20. [Chapter20: IMP - git branching model](Chapters/Chapter20-git%20branching%20model.md)
21. [Chapter21: IMP - Explaination on git branching](Chapters/Chapter21-Explaination%20on%20git%20branching%20model.md)

### Contact Me
- In the corresponding repository >> go to Plan >> Issues >> raise a New Issue
- inwincibleyou@gmail.com
- 8928885292

### Website
https://inwincibleshashank.in/