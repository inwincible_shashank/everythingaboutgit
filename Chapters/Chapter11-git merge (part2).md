## Revesion of simple git merge
![git merge Screenshot](../Screenshots/gitMergeConflict1.png)
![git merge Screenshot](../Screenshots/gitMergeConflict2.png)
![git merge Screenshot](../Screenshots/gitMergeConflict3.png)
![git merge Screenshot](../Screenshots/gitMergeConflict4.png)

## Merge conflict of 2 QA's working on same file
![git merge Screenshot](../Screenshots/gitMergeConflict5.png)
![git merge Screenshot](../Screenshots/gitMergeConflict6.png)
![git merge Screenshot](../Screenshots/gitMergeConflict7.png)
![git merge Screenshot](../Screenshots/gitMergeConflict8.png)
![git merge Screenshot](../Screenshots/gitMergeConflict9.png)
![git merge Screenshot](../Screenshots/gitMergeConflict10.png)
![git merge Screenshot](../Screenshots/gitMergeConflict11.png)
![git merge Screenshot](../Screenshots/gitMergeConflict12.png)

## Assignment:
AFter last screenshot where QA-1, QA-2 are 1 commit behind main branch using simple merge the head can be at same level perform this?