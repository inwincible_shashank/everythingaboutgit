## About git cherry-pick

'git cherry-pick' is a command that allows you to select specific commits from one branch and apply them to another branch. This is particularly useful when you want to include only certain changes from a feature branch or any other branch without merging the entire branch.

## Advantages of 'git cherry-pick' over 'git merge'
1. **Selective Integration**: With git cherry-pick, you can choose specific commits to integrate into your current branch, whereas git merge integrates all changes from the target branch.
2. **Avoid Unwanted Changes**: You can avoid including unwanted changes that might be present in the target branch.
3. **Conflict Isolation**: Resolving conflicts for specific commits can be easier and more isolated compared to handling conflicts that arise from a full merge.

## Scenario Setup
We have two branches: main and JIRA-3001. The JIRA-3001 branch has the following commits:
- Commit-1: Create the Login.html file.
- Commit-2: Add content to Login.html with Email, Password, and Submit button.
- Commit-3: Critical fix to encrypt the password field.
- Commit-4: Add CSS for Login.html.
- Commit-5: Create documentation for the login functionality.
We need to cherry-pick the critical fix (Commit-3) from JIRA-3001 to main.

## Scenario Implementation
![git cherry-pick Screenshot](../Screenshots/gitCherryPick1.png)
![git cherry-pick Screenshot](../Screenshots/gitCherryPick2.png)
![git cherry-pick Screenshot](../Screenshots/gitCherryPick3.png)
![git cherry-pick Screenshot](../Screenshots/gitCherryPick4.png)
![git cherry-pick Screenshot](../Screenshots/gitCherryPick.png)
![git cherry-pick Screenshot](../Screenshots/gitCherryPick5.png)
![git cherry-pick Screenshot](../Screenshots/gitCherryPick6.png)