## Clone of remote gitlab repo
```
git clone git@gitlab.com:inwincible_shashank/teachinggitcmdexecution/practise-git.git
```

## Uploading local project to gitlab repo
```
git init: intialise .git repo
```
- Establish connection between local project and remote git repo
- 'origin' is key and 'git@gitlab.....ecution/practise-git.git' is value
- When working on multiple repo key can be 'origin', 'originX' *(where X can be alpha-numeric characters)*
```
git remote add origin git@gitlab.com:inwincible_shashank/teachinggitcmdexecution/practise-git.git
```
```
git add .: bringing files from non-staging area to staging area
```
```
git commit -m "commit message": on 1st commit default branch will be created
```
```
git push -u origin main: creating new branch main and pushing code to remote repo
```
OR
- In case post 1st commit branch name is 'master' then create a branch, and push the code
```
git branch main: created new branch 'main'
```
```
git switch main: switched to main branch
```
- No need to enter remote repo gitlab path we can simply refer key 'origin'
```
git push origin main: local commited staging files uploaded to remote repo
```
```
git remote: To see list of active remote repo keys
```
```
git remote -v: To see detailed output of active remote repo keys and pairs
```
![git remote repo Screenshot](../Screenshots/remoteRepo8.png)
