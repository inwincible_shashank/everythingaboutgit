## Basic Info
- Branches are always going to be associated with commits
- Post 1st commit branch will be created (default - master)
- These are local branch

## git branch local cmds
```
git branch: display list of all local branches
```
```
git branch <branchName>: creates new local branch
```
This allows to work in isolation on a project and perform, validate changes independently before merging to master
```
git branch -d <branchName>: delete local branch
```
```
git switch <branchName>: switch to new branch
```
*instead 'switch' can also use 'checkout'*

## New Branch
- Multiple branches with head position on same commit are in sync
- Feature branch having additional commit are X commit ahead of main branch
*(where x is number of commits)*

## Reference screenshots:
![git branch Screenshot](../Screenshots/gitBranch1.png)
![git branch Screenshot](../Screenshots/gitBranch2.png)