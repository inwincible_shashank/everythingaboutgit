## git stash
```
git stash: Helps save modifications of a file already been tracked
```
```
git stash list: returns list of stash modifications
```
```
git stash apply <stash_Id>: apply the changes statshed respect to stash id provided
```
```
git stash clear: clears the temporary stashed list of modifications
```

![git stash Screenshot](../Screenshots/gitStash1.png)
![git stash Screenshot](../Screenshots/gitStash2.png)
![git stash Screenshot](../Screenshots/gitStash3.png)
![git stash Screenshot](../Screenshots/gitStash4.png)
![git stash Screenshot](../Screenshots/gitStash5.png)
![git stash Screenshot](../Screenshots/gitStash6.png)