## IMP git log cmds
```
git log --author="Shashank": commits by user shashank
```
```
git log --grep="Created": commits by keyword search
```
```
git log -n 2: latest two commits *(based on no. it will vary)*
```
```
git log --p: about changes creation/modification in commits
```
```
git log --oneline: commitId with one liner message
```

## Formatting commits info 
```
git log --pretty=short: short info about commits
```
```
git log --pretty=fuller: detailed info about commits
```
```
git log --pretty=oneline: one liner info about commits
```
```
git log --pretty=format:"%h %an %ae %s": display commits hash value, authorName, authorEmail, message
```

## Filter commits based on time
```
git log --since="yesterday": commits of yesterday
```
```
git log --since="2 week ago": commits of last 2 weeks
```
```
git log --since="3 month ago": commits of last 3 months
```
```
git log --since="2023-04-01" --until="2024-03-31": commits for date range (yyyy-mm-dd)
```



