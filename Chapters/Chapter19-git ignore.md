## .gitignore

- When we intialise the repo using git init after that do create this file
- Add files that are auto generated when run automation project
- Add dir that are auto generated when run automation project
- Please see if you create a xyz.file and then create this .gitignore file and add that xyz.file then git can't ignore it will continue to track
- So key here is to create .giignore file first and then continue creating rest of the files and dir

```
git status -u: to see untracked files
```
![git ignore Screenshot](../Screenshots/gitignore1.png)
![git ignore Screenshot](../Screenshots/gitignore2.png)
![git ignore Screenshot](../Screenshots/gitignore3.png)
![git ignore Screenshot](../Screenshots/gitignore4.png)
![git ignore Screenshot](../Screenshots/gitignore5.png)