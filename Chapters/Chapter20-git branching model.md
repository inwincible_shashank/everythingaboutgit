## Title
As an SDET, I want to collaborate with my team on a new project using GitLab, so that we can develop and integrate new features efficiently.

## Description
- As a member of the QA sun group, I want to create a new repository (project) and initialize it with a readme.md file in the main/master branch. I also want to protect the main/master branch to ensure its integrity.
- Next, I want to create a new integration branch, make it protected, and set it as the default branch. This will allow my team to work on new features and integrate them efficiently.
- As SDET-1, I want to create a new feature/login branch, add a new file, commit, and push the changes to the remote repository. Meanwhile, as SDET-2, I want to create a new feature/home branch, add a new file, and continue working on it locally without pushing the changes to the remote repository yet.
- When SDET-1's changes are reviewed and approved by the QA architect/lead, I want the changes to be merged into the integration branch. Then, as SDET-2, I want to switch to the integration branch, pull the latest changes, and receive the original readme.md file along with SDET-1's login file.
- If there are any merge conflicts, I want to resolve them efficiently. Then, I want to switch to my local feature/home branch, merge the changes from the integration branch, and continue working on the home file.
- Finally, I want to push the updated code to the remote feature/home branch, making it available for review and integration.

## Acceptance Criteria:
- A new repository (project) is created with a readme.md file in the main/master branch.
- The main/master branch is protected.
- A new integration branch is created, protected, and set as the default branch.
- SDET-1 can create a new feature/login branch, add a new file, commit, and push the changes to the remote repository.
- SDET-2 can create a new feature/home branch, add a new file, and continue working on it locally without pushing the changes to the remote repository yet.
- SDET-1's changes can be merged into the integration branch after review and approval.
- SDET-2 can switch to the integration branch, pull the latest changes, and receive the original readme.md file along with SDET-1's login file.
Merge conflicts can be resolved efficiently.
- SDET-2 can switch to the local feature/home branch, merge the changes from the integration branch, and continue working on the home file.
The updated code can be pushed to the remote feature/home branch.

## Workflow diagram
![git branching workflow Screenshot](../Screenshots/Git%20Branching%20Model.png)

## Gitlab Screenshots
![git branching Screenshot](../Screenshots/gitBranchingDemo1.png)
![git branching Screenshot](../Screenshots/gitBranchingDemo2.png)
![git branching Screenshot](../Screenshots/gitBranchingDemo3.png)
![git branching Screenshot](../Screenshots/gitBranchingDemo4.png)
![git branching Screenshot](../Screenshots/gitBranchingDemo5.png)
![git branching Screenshot](../Screenshots/gitBranchingDemo6.png)
![git branching Screenshot](../Screenshots/gitBranchingDemo7.png)
![git branching Screenshot](../Screenshots/gitBranchingDemo8.png)
![git branching Screenshot](../Screenshots/gitBranchingDemo9.png)
![git branching Screenshot](../Screenshots/gitBranchingDemo10.png)
![git branching Screenshot](../Screenshots/gitBranchingDemo11.png)
![git branching Screenshot](../Screenshots/gitBranchingDemo12.png)
![git branching Screenshot](../Screenshots/gitBranchingDemo13.png)
![git branching Screenshot](../Screenshots/gitBranchingDemo14.png)

## Assignment
1. Remaining steps:
- Post pull follow above scenario steps and push feature/home branch to remote

2. Handling conflicts
- Again switch to feature/login create a new file 'config.env' and push to remote feature branch
- M.R. to default integration branch
- Take a pull on local integration branch
- Switch to feature/home and again merge from integration branch
- Resolve conflicts by accepting all files
- Edit and update 'config.env' and push changes to remote branch
- Accept new M.R. to integration branch
- Again take a pull on local integration branch
- And keep the other brach feature/login now merged with latest M.R. accepted