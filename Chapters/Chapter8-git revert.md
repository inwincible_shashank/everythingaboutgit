## git revert

- git revert is used to create a new commit that undoes the changes of a specified commit
- It does not change the commit history but appends a new commit that negates the effects of the specified commit

```
git revert <commitId>
```
*P.S. you can always use below cmd to see undone changes using git revert*
```
git checkout <commitId>: To see reverted changes as it is not lost
```

## To demonstrate git revert below prepared test data
![test data Screenshot](../Screenshots/testData1.png)
![test data Screenshot](../Screenshots/testData2.png)
![test data Screenshot](../Screenshots/testData3.png)
![test data Screenshot](../Screenshots/testData4.png)

## Working of git revert

1. Commit-1: Created 'listFood.html' with 5 items
2. Commit-2: Created 'grocery.html' with 5 items
3. Commit-3: Modified 'listFood.html' with additional 3 items and total as 8 items
4. Commit-4: Created 'Restaurant.html' with 10 restaurants
5. Commit-5: Modified 'grocery.html' with additional 3 items and total as 8 items
6. Commit-6: Created 'offers.html' with 5 offers listed

```
git revert commit-6
```
- A new revert commit will be created i.e. Commit-7
- This will undone the changes for Commit-6, so the file 'offers.html' will not be displayed in the swiggy dir

```
git revert commit-5
```
- A new revert commit will be created i.e. Commit-8
- So this will undone the 3 additional items added in grocery.html
- Only original 5 items will be there in the file which were done in Commit-2

## Reference screenshots:
![git revert Screenshot](../Screenshots/gitrevert1.png)
![git revert Screenshot](../Screenshots/gitrevert2.png)
![git revert Screenshot](../Screenshots/gitrevert3.png)