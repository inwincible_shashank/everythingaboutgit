## git files working of staging area, non-staging area

```
git add fileName: to bring file from non-staging area to staging area
```
```
git add .: to bring multiple files from non-staging area to staging area
```
```
git rm --cached file1 file2 file3: to bring multiple files from staging area area to non-staging area
```

*P.S. adding all files in one go not recommended unless code is compiled with zero errors*

## Reference screenshots:
![git add Screenshot](../Screenshots/gitadd1.png)
![git add Screenshot](../Screenshots/gitadd2.png)
![git add Screenshot](../Screenshots/gitadd3.png)

