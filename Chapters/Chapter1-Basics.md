## Git Basics

### Account setup

1. The very first step is to setup your user info into the system using git bash
```
git config --global user.name "shashank shetkar"
git config --global user.email "shetkarshashank@gmail.com"
```
*(please enter your respective user name and email address)*

2. To intialize Local git repo (.git is a hidden folder) 
```
git init
```

*(For further commands create a new file using cmd: 'touch file1.txt')*

3. To identify untracked files
```
git status
```

4. To bring files from non-staging area to staging area
```
git add fileName
```

5. To track files
```
git commit
```
Now inside vi editor use 'i' to go into INSERT mode and then add commit message and to save, exit the editor use ':wq'

6. To check list of commits
```
git log
```

### Exercise: Now perfrom all above cmds except intializing git repo

## Reference screenshots:
![Basics Screenshot](../Screenshots/Basics.png)
![Basics Screenshot](../Screenshots/Basics1.png)
![Basics Screenshot](../Screenshots/Basics2.png)
![Basics Screenshot](../Screenshots/Basics3.png)
![Basics Screenshot](../Screenshots/Basics4.png)