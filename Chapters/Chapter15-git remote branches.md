## git branch remote cmds
```
git branch -r: display list of all remote branches
```
```
git push origin <branch-name>: push a branch to the remote
```
```
git switch origin/<branchName>: switch to remote branch
```
```
git fetch: fetch all branches from the remote
```
```
git push origin -d <branch-name>: delete a remote branch
```
```
git pull origin <branch-name>: pull changes from the remote
```
```
git fetch --prune: deletes local branches that does not exist on remote
```

## Reference screenshots:
![git remote branch Screenshot](../Screenshots/gitRemoteBranch1.png)
![git remote branch Screenshot](../Screenshots/gitRemoteBranch2.png)
![git remote branch Screenshot](../Screenshots/gitRemoteBranch3.png)