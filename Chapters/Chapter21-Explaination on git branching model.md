## Branching model

1. In our automation framework we try to follow 4 eye principles so that we can ensure that there is **High Code Quality** and **Knowledge sharing is easy**
2. During the start of the Sprint the integration branch is updated from master.
3. Both Integration and Master are protected branches and apart from maintainer of our project (our Team Lead) no one is allowed to MERGE into the integration and master
4. Once our Integration branch is updated from master. We take a fetch from Remote Integration Branch and create our respective feature branches.
5. Special attention is given to the naming convention of our feature branches so that a standardization is followed by all our teammate.
6. All these rules are set in our remote repository in Gitlab under settings options.
7. Once we complete our creation of test scripts in our respective feature branches, we push it to the remote repository.
8. **Before doing a push we always check if the integration branch is updated by doing a git fetch**
9. This is done to make sure we are always working with the latest piece of code, and it helps in avoid conflicts in future.
10. In GitLab we raise our Merge request to merge our feature branch to integration branch
11. This merge request is reviewed our colleague or team lead and our test script is reviewed
12. During code review we usually focus on
- If OOPs is implemented properly
- Naming Convention for Classes/Variables and Function
- Function Body and Class Body
- LOGIC of course!
13. If the reviewer finds the code proper, It will be merged into the integration branch and all the teams mates are notified via email and slack that they need to take a fetch from integration branch.
14. If the review rejects it, then we fix the changes as per the suggestion and raise a new Merge request.
15. **Before doing a push we always check if the integration branch is updated by doing a git fetch**
16. Once we got all the major features in integration branch. Then integration branch is
merged with Master branch
17. **We use Master branch with Jenkins for continuous Testing of the API/ UI and Android project**

## Benefit of a well-disciplined git branching model
- 4 Eyes Principles
- Better Knowledge Sharing
- Less Conflicts
- Complete Isolation of Work