## Creating subgroup on gitlab
![git subgroup Screenshot](../Screenshots/remoteSubgroup.png)
![git subgroup Screenshot](../Screenshots/remoteSubgroup1.png)
![git subgroup Screenshot](../Screenshots/remoteSubgroup2.png)
![git subgroup Screenshot](../Screenshots/remoteSubgroup3.png)
![git subgroup Screenshot](../Screenshots/remoteSubgroup4.png)
![git remote Add Screenshot](../Screenshots/remoteRepo1.png)

## Taking git clone of remote repo
- Suppose your office team has an existing automation project on a gitlab/ github/ bitbucket repository
- Then to work on that project you need to download on your local laptop
- So locally you will create the same project name folder
- And then you will simply **clone** the project
![git clone Screenshot](../Screenshots/gitClone1.png)
![git clone Screenshot](../Screenshots/gitClone2.png)
![git clone Screenshot](../Screenshots/gitClone3.png)

## Uploading project built from scratch
- Suppose in your org. you are developing automation project
- And you want to upload your project files
- Then you will use **git remote add** with key as 'origin' and value as 'gitlab repo url'
- Then on regular basis you can continue to develop project on your local and use **git push** to upload modified changes to the project
![git remote Add Screenshot](../Screenshots/remoteRepo1.png)
![git remote Add Screenshot](../Screenshots/remoteRepo2.png)
![git remote Add Screenshot](../Screenshots/remoteRepo3.png)
![git remote Add Screenshot](../Screenshots/remoteRepo4.png)
![git remote Add Screenshot](../Screenshots/remoteRepo5.png)
![git remote Add Screenshot](../Screenshots/remoteRepo6.png)
![git remote Add Screenshot](../Screenshots/remoteRepo7.png)