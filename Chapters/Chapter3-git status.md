## git status attributes

```
git status: to identify untracked files
```

```
git status -s: to see short information 1 liner
```
```
git status -v: to see detailed output
```

## Reference screenshots:
![git status Screenshot](../Screenshots/gitstatus1.png)
![git status Screenshot](../Screenshots/gitstatus2.png)