## Simple merge

- Make sure you are in active branch in which you have to merge code from another branch
- For this use cmd: git checkout or git switch
- Once the git merge cmd is executed then another branch commits are all synced in present branch

```
git merge <branchName>
```
![git merge Screenshot](../Screenshots/gitMerge1.png)

## Merge of 2 QA's with seperate files
1. There is a default 'main' branch created after 1st commit (CMN1)
2. Then in main branch few more commits are performed (CMN2, CMN3)

head is pointing to CMN3 of main branch

3. At CMN3 commit of main branch 2 seperate branches are created 'JIRA-1001', 'JIRA-1002'

head is pointing to CMN3 of branch - main, JIRA-1001, JIRA1002

3. QA-1 switches to branch 'JIRA-1001', commits changes and commitId is (CQA1)
4. QA-2 switches to branch 'JIRA-1002', commits changes and commitId is (CQA2)
5. Now, QA-1 switches to main branch and merge the commit (CQA1)

head is pointing to commit (CQA1) of branch - main, JIRA-1001

6. This will result in change in source of main branch from which QA-2 started work

so for QA-2 while starting work on new branch head was pointing to (CMN3) but after QA-1 merge its pointing to (CQA1)

7. So when QA-2 switches to main head position is at (CQA1)
8. ANd when performing merge a new commit gets created with all changes incorporating QA-1, QA-2 and main

![git merge Screenshot](../Screenshots/gitMerge2.png)
![git merge Screenshot](../Screenshots/gitMerge3.png)
![git merge Screenshot](../Screenshots/gitMerge4.png)
![git merge Screenshot](../Screenshots/gitMerge5.png)
![git merge Screenshot](../Screenshots/gitMerge6.png)
![git merge Screenshot](../Screenshots/gitMerge7.png)
![git merge Screenshot](../Screenshots/gitMerge8.png)
![git merge Screenshot](../Screenshots/gitMerge9.png)