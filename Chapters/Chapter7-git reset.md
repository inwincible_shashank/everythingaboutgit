```
git reset --soft: Resets the commit history, keeps changes in the staging area
```
```
git reset --mixed: Resets the commit history and the staging area, keeps changes in the working directory *(non-staging area)*
```
```
git reset --hard: Resets the commit history, staging area, and working directory *(changes are lost)*
```
*Never use this when working in collaboration with other SDET in org. projects. A good practise is to use [git revert](/Chapters/Chapter8-git%20revert.md)*

## Reference screenshots:
![git reset Screenshot](../Screenshots/gitreset1.png)
![git reset Screenshot](../Screenshots/gitreset2.png)
![git reset Screenshot](../Screenshots/gitreset3.png)