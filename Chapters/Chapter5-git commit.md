## About git commit

- git commit done using SHA-1(secure hash algo-1) encryption technique and a snapshot gets created of that commit
- Commit id is a 40 char of alpha-numeric consisting of below info:
    - who? username
    - when? date/time
    - msg? Entered commit message
    - snpshot? files in the staging area

- To see snapshot info navigate to .git >> objects >> commit id (starting 2 char) >> inside that 40 char commit id can be found

- Head is a pointer to latest commit but we can change the position of head
- On 1st commit branch gets created (master)
- For 2nd commit onwards the commit have reference of previous commit pointer
*e.g. 1st commit: index.html is added, commited
2nd commit: website.log is added then it has reference of 1st commit + website.log  (i.e. website.log + index.html)*

## [QQ] In an existing commit if we want to make modifications then how to do that? *Refer Screenshots*
```
git commit --amend
```
```
git commit --amend --no-edit
```

## [QQ] How to provide sign-off commit upon project completion? *Refer Screenshots*
```
git commit -s -m "commit message"
```

## [QQ] How to perform empty git commits? *Refer Screenshots*
```
git commit --allow-empty -m "commit message"
```
## [QQ] Manager wants to provide empty sign-off git commit? *Refer Screenshots*
```
git commit --allow-empty -s -m "commit message"
```

## Reference screenshots:
![git existingCommit Screenshot](../Screenshots/gitCommit.png)
![git existingCommit Screenshot](../Screenshots/gitCommit1.png)
![git existingCommit Screenshot](../Screenshots/gitCommit2.png)
![git existingCommit Screenshot](../Screenshots/gitCommit3.png)
![git existingCommit Screenshot](../Screenshots/gitCommit4.png)
![git existingCommit Screenshot](../Screenshots/gitCommit5.png)
![git existingCommit Screenshot](../Screenshots/gitCommit6.png)