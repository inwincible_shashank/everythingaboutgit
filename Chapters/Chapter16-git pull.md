## git pull = git fetch + git merge
```
git pull: download and merge (local + remote) commits
```
```
git fetch: verify remote commits if any additional commits 
```
```
git merge: merge (local + remote) commits
```

## Feature: Demonstrate git pull

  Scenario: SDET-2 takes pull from main after SDET-1 merges code to main remote

    Given the branch - 'main', 'JIRA-5001-LOGIN', and 'JIRA-5002-HOME' are created and pushed to remote
    And all branches' heads are on the same level
    And on main, a QA architect creates a file pom.xml and commits it to the local and remote branch
    And SDET-1 checks out branch 'JIRA-5001-LOGIN'
    And SDET-2 checks out branch 'JIRA-5002-HOME'
    And SDET-1 creates an angular file for login and makes 2 commits
    And SDET-1 merges the code to the main local and remote branch
    And SDET-2 continues working on the angular file for home and makes 3 commits
    When SDET-2 takes a pull from the main remote branch
    Then SDET-2's branch will contain the latest changes from the main branch
    And SDET-2's local commits will be retained

*P.S.in below screenshots the branches shown in <span style="color:green;">**green color**</span> are local branches whereas branches shown <span style="color:red;">**origin/&lt;branch-name&gt;**</span> are remote branches with <span style="color:red;">**red color**</span>*
- Screenshot- 1 to 5 demonstarte simple git merge
- Screenshot 6 onwards demonstarte git pull scenario

![git pull Screenshot](../Screenshots/gitPull1.png)
![git pull Screenshot](../Screenshots/gitPull2.png)
![git pull Screenshot](../Screenshots/gitPull3.png)
![git pull Screenshot](../Screenshots/gitPull4.png)
![git pull Screenshot](../Screenshots/gitPull5.png)
![git pull Screenshot](../Screenshots/gitPull6.png)
![git pull Screenshot](../Screenshots/gitPull7.png)
![git pull Screenshot](../Screenshots/gitPull8.png)
![git pull Screenshot](../Screenshots/gitPull9.png)
![git pull Screenshot](../Screenshots/gitPull10.png)
![git pull Screenshot](../Screenshots/gitPull11.png)
![git pull Screenshot](../Screenshots/gitPull12.png)
![git pull Screenshot](../Screenshots/gitPull13.png)
![git pull Screenshot](../Screenshots/gitPull14.png)