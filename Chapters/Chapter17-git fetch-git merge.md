## Feature: Demonstrate git fetch+git merge v/s git pull

  Scenario: SDET-2 takes pull from main after SDET-1 merges code leads to loss of files and conflicts

    "Given QA architect creates a config.env file and commits the changes
    And branch - 'main', 'JIRA-6001-Address' are created and pushed to remote
    And all branches' heads are on the same level
    And SDET-1 checks out branch 'JIRA-6001-Address'
    And SDET-2 checks out branch 'JIRA-6001-Address'
    And SDET-1 creates an angular file for ShippingAddress and makes 1 commit
    And SDET-1 modifies the file 'config.env' and makes another commit
    The QA architect merges the code of SDET-1 to the main local and remote branch
    And SDET-2 checkout new branch 'JIRA-6002-Address'
    And modifies same file 'config.env' and adds 1 commit for 'config.env'
    And adds another commit for file BillingAddress and makes 1 commit
    And SDET-1 further modifies the file, commits and pushes the code, meanwhile SDET-2 continue to work on the same file

#### Git Pull Scenario
- When SDET-2 takes a pull from main
- Then SDET-2's branch will contain the latest changes from the main branch
- <span style="color:red;">**But SDET-2's local commits will be lost**</span>

#### Git Fetch + Git Merge Scenario
- When SDET-2 fetches the latest changes from main
- And SDET-2 merges the fetched changes into their local branch
- Then SDET-2's branch will contain the latest changes from the main branch
- <span style="color:green;">**And SDET-2's local commits will be retained**</span>

![git fetchmerge Screenshot](../Screenshots/gitFetchMerge1.png)
![git fetchmerge Screenshot](../Screenshots/gitFetchMerge2.png)
![git fetchmerge Screenshot](../Screenshots/gitFetchMerge3.png)
![git fetchmerge Screenshot](../Screenshots/gitFetchMerge4.png)
![git fetchmerge Screenshot](../Screenshots/gitFetchMerge5.png)
![git fetchmerge Screenshot](../Screenshots/gitFetchMerge6.png)
![git fetchmerge Screenshot](../Screenshots/gitFetchMerge7.png)
![git fetchmerge Screenshot](../Screenshots/gitFetchMerge8.png)
![git fetchmerge Screenshot](../Screenshots/gitFetchMerge9.png)
![git fetchmerge Screenshot](../Screenshots/gitFetchMerge10.png)