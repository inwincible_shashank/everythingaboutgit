## Folder staructure:

- Parent Folder: everythingaboutgit
- Child hidden folders:
    - .git
- Child folders:
    - Chapters
    - Practise git
    - Screenshots
- Child files: 
    - logo.png
    - README.md

### IMP Note:
- When we want to track project files, folders do NOT initialise '.git' folder for Parent as well as Child. Either do it for Parent folder or Child folder.
- I need to upload all my project files, folders inside 'everythingaboutgit' hence I have created '.git' folder inside that folder level.
- But when you are practising these commands create 'Practise git' a Parent folder And create '.git' folder inside that

## git init attributes
```
git init: to intialise local git repo
```
```
rm -rf .git: to remove intialised local git repo
```
*the above is the linux cmd*

## Reference screenshots:
![git init Screenshot](../Screenshots/gitinit1.png)
![git init Screenshot](../Screenshots/gitinit2.png)